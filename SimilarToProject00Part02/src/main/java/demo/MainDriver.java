package demo;

import java.util.Scanner;

import demo.menu.MenuManager;
import demo.model.Account;
import demo.service.SuperHumanService;
import demo.service.SuperHumanServiceImpl;
import demo.service.SuperPowerService;
import demo.service.SuperPowerServiceImpl;

public class MainDriver {

	//I need references to the service classes
	public static SuperHumanService myHumanService = new SuperHumanServiceImpl();
	public static SuperPowerService myPowerService = new SuperPowerServiceImpl();
	
	//user that is currently logged in
	public static Account currentUser= null;
	
	
	
	
	public static void main(String[] args) {
		//we need to take in input from our user, so we need a scanner class
		Scanner myScanner = new Scanner(System.in);

		
		//let's make an event loop for our application
		boolean loopActive = true;
		
		while(loopActive) {
			//logic
			MenuManager.printMainMenu();
			
			String stringInput = myScanner.nextLine();
			

			switch(stringInput) {
			case "0":
			case "exit":
				loopActive = false;
				break;
				
			case "1":
			case "register":
				//not going to do this during this example
				break;
				
			case "2":
			case "login":
				MenuManager.loginPrompt(myScanner);
				break;
			}
			
			if(currentUser != null) {
				loggedInEventLoop(myScanner);
			}
			
		}
		
		System.out.println("\n\n\n---------------PROGRAM TERMINATED---------------\n\n\n");
	}
	
	
	
	
	
	public static void loggedInEventLoop(Scanner myScanner) {

		//IF THEY DO LOG IN
		while(currentUser != null){
			
			MenuManager.printLoggedInMenu();
			
			//notice how I am doing "nextLine()" to take in all lines as strings
			String stringInput = myScanner.nextLine();
			
			switch(stringInput) {
			case "0":
			case "logout":
				MainDriver.currentUser = null;
				break;
				
			case "1":
			case "powers":
			case "select all powers":
				System.out.println(myPowerService.findAllPowers());
				break;
				
			case "2":
			case "humans":
			case "select all humans":
				System.out.println(myHumanService.findAllHumansOneDBQuery());
				break;
				
			case "7":
			case "delete":
			case "delete human":
				Integer deletePK = MenuManager.separateMethodForScannerIntInput(
						myScanner, "What is the PK of the human you'd like to delete?");
				
				if(deletePK !=null) {
					boolean results= myHumanService.removeHuman(deletePK);
					
					if(results)
						System.out.println("Delete went according to plan");
					else {
						System.out.println("Sorry, the delete failed. Try again. Taking you back to the main menu.");
						continue;
					}
				}
				else
					continue;
				
			case "9":
			case "space":
				System.out.println("\n\n\n------------------------------\n\n\n");
				break;
			}

		}
	}
	
	
}
