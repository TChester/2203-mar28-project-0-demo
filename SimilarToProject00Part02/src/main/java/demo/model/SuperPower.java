package demo.model;

public class SuperPower {

	//these variables are mirroring the DB talbes
	private int spowerId;
	private String spowerName;
	
	public SuperPower() {
	}

	public SuperPower(int spowerId, String spowerName) {
		super();
		this.spowerId = spowerId;
		this.spowerName = spowerName;
	}

	public int getSpowerId() {
		return spowerId;
	}

	public void setSpowerId(int spowerId) {
		this.spowerId = spowerId;
	}

	public String getSpowerName() {
		return spowerName;
	}

	public void setSpowerName(String spowerName) {
		this.spowerName = spowerName;
	}

	@Override
	public String toString() {
		return "\nSuperPower [spowerId=" + spowerId + ", spowerName=" + spowerName + "]";
	}
	
	
}
