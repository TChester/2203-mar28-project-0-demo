package demo.model;

import java.util.ArrayList;
import java.util.List;

public class SuperHuman {

	//these variables are mirroring the DB tables
	private int shumanId;
	private String shumanName;
	private int bounty;
	
	/*
	 * THIS NEXT VARIABLES IS OPTIONAL...it's also the reason why my DAO is going to be very complex
	 * 
	 * this next variable is NOT in the superhuman table...but I don't want to bring in the
	 * junction tables, so i'll bring in the superpowers with each villain/superhuman
	 */
	private List<SuperPower> arrayListOfPowers = new ArrayList<SuperPower>(); //I don't want this to start as "null"
	
	public SuperHuman() {
	}
	
	public SuperHuman(int shumanId, String shumanName, int bounty) {
		super();
		this.shumanId = shumanId;
		this.shumanName = shumanName;
		this.bounty = bounty;
	}

	public SuperHuman(int shumanId, String shumanName, int bounty, List<SuperPower> arrayListOfPowers) {
		super();
		this.shumanId = shumanId;
		this.shumanName = shumanName;
		this.bounty = bounty;
		this.arrayListOfPowers = arrayListOfPowers;
	}

	public int getShumanId() {
		return shumanId;
	}

	public void setShumanId(int shumanId) {
		this.shumanId = shumanId;
	}

	public String getShumanName() {
		return shumanName;
	}

	public void setShumanName(String shumanName) {
		this.shumanName = shumanName;
	}

	public int getBounty() {
		return bounty;
	}

	public void setBounty(int bounty) {
		this.bounty = bounty;
	}

	public List<SuperPower> getArrayListOfPowers() {
		return arrayListOfPowers;
	}

	public void setArrayListOfPowers(List<SuperPower> arrayListOfPowers) {
		this.arrayListOfPowers = arrayListOfPowers;
	}

	@Override
	public String toString() {
		return "\nSuperHuman [shumanId=" + shumanId + ", shumanName=" + shumanName + ", bounty=" + bounty
				+ ", arrayListOfPowers=" + arrayListOfPowers + "]";
	}

	
}
