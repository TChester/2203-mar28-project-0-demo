package demo.dao;

import java.util.List;

import demo.model.SuperHuman;

public interface SuperHumanDao {

	//inserts
	public boolean insertHumanWithoutPowers(SuperHuman myHuman);
	public boolean insertHumanWithPowers(SuperHuman myHuman);
	
	//selects
	public SuperHuman selectHumanById(int humanId);
	public List<SuperHuman> selectAllHumansMultipleDBQueries();
	public List<SuperHuman> selectAllHumansOneDBQuery();
	
	//delete
	public boolean deleteHuman(int targetPrimaryKey);
	
	
}
