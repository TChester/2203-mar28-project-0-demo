package demo.dao;

import java.util.List;

import demo.model.SuperPower;

public interface SuperPowerDao {

	//inserts
	public boolean insertPower(String powerName);
	
	//selects
	public List<SuperPower> selectAllPowers();
	public SuperPower selectPowerById(int powerId);
	public SuperPower selectPowerByName(String powerName);
	public List<SuperPower> selectAllPowersFromSpecificSuperHuman(String sh_name);
}
