package demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import demo.model.SuperHuman;
import demo.model.SuperPower;

public class SuperHumanDaoImpl implements SuperHumanDao {

	@Override
	public boolean insertHumanWithoutPowers(SuperHuman myHuman) {

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {

			String sql = "INSERT INTO superhumans (shuman_name, bounty) VALUES (?,?)";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, myHuman.getShumanName());
			ps.setInt(2, myHuman.getBounty());

			ps.executeUpdate(); // <----update not query. This line sends our statement to the DB

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public boolean insertHumanWithPowers(SuperHuman myHuman) {
						///for this method I assumed that the user knows the NAME of the powers, but
						// NOT the power's PK
		
		/*
		 * FOR THE LOVE OF ALL I KNOW....THE HUMAN MIND CANNOT SOLVE COMPLEX PROBLEMS!!!!
		 * 
		 * Please divide up the problem into easy to digest pieces. Do NOT try to do intense
		 * logic like this in your head.
		 */

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {

			//STEP 1: insert the superhuman AND get the generated surrogate key back from the Database
			
			//STEP 1.1: create our sql string
			String sql = "INSERT INTO superhumans (shuman_name, bounty) VALUES (?,?)";

			//STEP 1.2: prepare our statement
			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, myHuman.getShumanName());
			ps.setInt(2, myHuman.getBounty());

			//STEP 1.3: send our statement to the DB
			ps.executeUpdate(); // <----update not query.
			
			/////
			
			//STEP 2: we need the generated PK of the new superhuman, for later calculations
			//THIS section is necessary to return the PK of the inserted record
			
			//STEP 2.1: create a holder variable for the PK
			int generatedPK = -1; //this is a holder variable for said PK
			
			//STEP 2.2: get the result set object
			ResultSet rs = ps.getGeneratedKeys();
			
			//STEP 2.3: extract the data from the result set
			if(rs.next()) {
				generatedPK = rs.getInt("shuman_id");
			}
			
			/////
			
			//STEP 3: let's insert the superhuman's powers into the DB as well, via the junction table
			SuperPowerDao mySPowerDao = new SuperPowerDaoImpl();
			
			for(int i=0; i< myHuman.getArrayListOfPowers().size(); i++) {
				//STEP 3.1: Let's create our SQL string
				String step2SqlStatement = "INSERT INTO shuman_spower_junction VALUES (?,?)";
				
				//STEP 3.2: LET'S NOW GET THE POWER'S PK (note: this logic assumes
									///the power already exists in the power table)
				String targetPowerName = myHuman.getArrayListOfPowers().get(i).getSpowerName();
				SuperPower extractedPower = mySPowerDao.selectPowerByName(targetPowerName);
				int powerPK = extractedPower.getSpowerId();
				
				
				//STEP 3.3: now, let's finally insert THIS specific junction table pair
				PreparedStatement ps2 = conn.prepareStatement(step2SqlStatement);
				ps2.setInt(1, generatedPK);
				ps2.setInt(2, powerPK);
				
				ps2.executeUpdate();
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	@Override
	public SuperHuman selectHumanById(int humanId) {

		SuperHuman targetHuman = null;

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {

			//STEP 1: go find the superhuman
			String sql = "SELECT * FROM superhumans WHERE shuman_id=?";

			//STEP 1.2: prepare statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, humanId);

			ResultSet rs = ps.executeQuery(); // <----query not update
			
			//STEP 1.3: extra data from the result set
			if(rs.next()) {
				targetHuman = new SuperHuman(
							rs.getInt("shuman_id"),
							rs.getString("shuman_name"),
							rs.getInt("bounty")
						);
			}
			
			/////
			
			//STEP 2: go find all the superhuman's powers from the junction & power table
			if(targetHuman != null) {
				SuperPowerDao mySPowerDao = new SuperPowerDaoImpl();
				
				//STEP 2.2: ask the superpower dao to give us that human's powers
				List<SuperPower> thisHumansPowers =
						mySPowerDao.selectAllPowersFromSpecificSuperHuman(targetHuman.getShumanName());
				
				//STEP 2.3: add that superhuman's powers to the superhuman object
				targetHuman.setArrayListOfPowers(thisHumansPowers);
				
			}
			

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return targetHuman;
	}
	

	

	@Override
	public List<SuperHuman> selectAllHumansMultipleDBQueries() {
		
		List<SuperHuman> myHumanList = new ArrayList<>();

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {

			//////STEP 1: go find all the superhumans
			String sql = "SELECT * FROM superhumans";

			//STEP 1.2: prepare statement
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery(); // <----query not update
			
			//STEP 1.3: extra data from the result set
			while(rs.next()) {
				myHumanList.add( new SuperHuman(
							rs.getInt("shuman_id"),
							rs.getString("shuman_name"),
							rs.getInt("bounty")
						));
			}
			
			/////
			System.out.println(myHumanList);
			
			//STEP 2: go find all the superhuman's powers from the junction and power tables
			SuperPowerDao mySPowerDao = new SuperPowerDaoImpl();
			
			for(int i=0; i<myHumanList.size(); i++){			
				//STEP 2.2: find current human in the arraylist
				SuperHuman currentHuman = myHumanList.get(i);
				
				//STEP 2.3: ask the superpower dao to give us that human's powers
				List<SuperPower> thisHumansPowers=
						mySPowerDao.selectAllPowersFromSpecificSuperHuman(currentHuman.getShumanName());
				
				//STEP 2.4: add that human's powers to the superhuman object
				currentHuman.setArrayListOfPowers(thisHumansPowers);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return myHumanList;
	}

	@Override
	public List<SuperHuman> selectAllHumansOneDBQuery() {
		List<SuperHuman> humansList = new ArrayList<>();

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {

			//STEP 1: get the joined result set of ALL superhumans and their powers (join includes junction table)
			String sql = "SELECT * " + 
					"FROM superhumans A " + 
					"INNER JOIN shuman_spower_junction B " + 
					"ON A.shuman_id = B.my_shuman_id " + 
					"INNER JOIN superpowers C " + 
					"ON C.spower_id = B.my_spower_id "+
					"ORDER BY shuman_name";

			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery(); // <--query not update

			//STEP 2: we need to interpret this mess of a result set
			SuperHuman previousHuman = null;
			
			while (rs.next()) {
				SuperHuman currentHuman = new SuperHuman(rs.getInt("shuman_id"), rs.getString("shuman_name"),
						rs.getInt("bounty"), new ArrayList<SuperPower>());
				
				//let's get the names of the previous human and the current human
				String currentName= currentHuman.getShumanName();
				String previousHumanName= null;
				
				if(previousHuman != null)
					previousHumanName= previousHuman.getShumanName();
				
				if(previousHuman!= null && currentName.equalsIgnoreCase(previousHumanName)) {
					//THIS BLOCK TRIGGERS WHEN THE CURRENT SUPERHUMAN IS ALREADY IN THE RETURN ARRAYLIST
					
					
					//if the previous human is the same as the current human then we don't need to add
					// the current huaman to the return arraylist AGAIN...we just need to add the new power to
					// the existing human object
					List<SuperPower> previousPowers = previousHuman.getArrayListOfPowers();
					previousPowers.add(new SuperPower(rs.getInt("spower_id"), rs.getString("spower_name")));
				}else {
					//THIS BLOCK TRIGGERS WHEN THE CURRENT SUPERHUMAN NOT CURRENTLY IN THE RETURN ARRAYLIST
					
					
					//let's set the previous human equal to the current human we're analyzing
					previousHuman = currentHuman;
					List<SuperPower> currentPowers = currentHuman.getArrayListOfPowers();
					currentPowers.add(new SuperPower(rs.getInt("spower_id"), rs.getString("spower_name")));
					
					//let's add the current human to our return arraylist
					humansList.add(currentHuman);
				}
			}
			
			

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return humansList;
	}

	public static void main(String[] args) {
		SuperHumanDaoImpl myDao = new SuperHumanDaoImpl();
//		System.out.println(myDao.selectHumanById(1));
		System.out.println(myDao.selectAllHumansMultipleDBQueries());
//		myDao.deleteHuman(1);
	}

	
	@Override
	public boolean deleteHuman(int targetPrimaryKey) {

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {

			//STEP 1: delete the children records FIRST!!! In our case, the junction table records
			String sql = "DELETE FROM shuman_spower_junction WHERE my_shuman_id =?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, targetPrimaryKey);

			int numOfRowsEffected = ps.executeUpdate(); // <----update not query.
			System.out.println("Num of rows affected during deletion of parent: "+numOfRowsEffected);
			
			//STEP2: delete the parent record!
			String sqlTwo = "DELETE FROM superhumans WHERE shuman_id =?";

			PreparedStatement psTwo = conn.prepareStatement(sql);
			psTwo.setInt(1, targetPrimaryKey);

			psTwo.executeUpdate(); // <----update not query.
			

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
