package demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import demo.model.SuperPower;

public class SuperPowerDaoImpl implements SuperPowerDao {

	
	//manual testing of this class
	//THE HUMAN MIND CANNOT SOLVE COMPLEX PROBLEMS
	public static void main(String[] args) {
		SuperPowerDaoImpl myDao = new SuperPowerDaoImpl();
//		System.out.println(myDao.selectAllPowersFromSpecificSuperHuman("Danny Boy"));
//		System.out.println(myDao.selectPowerByName("Toy Chest"));
//		System.out.println(myDao.selectPowerById(5));
//		System.out.println(myDao.insertPower("Java Power"));
		System.out.println(myDao.selectAllPowers());
	}
	
	
	
	
	@Override
	public boolean insertPower(String powerName) {
		//this is called a "try with resources"
		try(Connection conn = OurCustomConnectionFactory.getConnection()) {
			
							//you may also hardcode values into your sql string if you'd like
			///for exameple "INSERT INTO pokemon VALUES(DEFAULT,?,?,'normal')"
			///for exameple "INSERT INTO pokemon VALUES(15 ,?,?, null)"
			String ourSQLStatement = "INSERT INTO superpowers (spower_name) VALUES (?)";

			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setString(1, powerName);
			
			int numOfRowsEffected = ps.executeUpdate(); //<---update not query.
			
			if(numOfRowsEffected==1)
				return true;
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public List<SuperPower> selectAllPowers() {

		// my arraylist
		List<SuperPower> powers = new ArrayList<>();

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {
			String sql = "SELECT * FROM superpowers";

			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery(); // <---query not update. this line is what sends the information to the DB

			while (rs.next()) {

				// the following two adds do the same thing, you can extract information using
				// the column orders
				// OR using the column names

//				powers.add(new SuperPower(rs.getInt(1), rs.getString(2)));
				powers.add(new SuperPower(rs.getInt("spower_id"), rs.getString("spower_name")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return powers;
	}

	@Override
	public SuperPower selectPowerById(int powerId) {
		// target power
		SuperPower targetPower = null;

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {
			String sql = "SELECT * FROM superpowers WHERE spower_id=?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, powerId);

			ResultSet rs = ps.executeQuery(); // <---query not update. this line is what sends the information to the DB

			if (rs.next()) {

				// the following two adds do the same thing, you can extract information using
				// the column orders
				// OR using the column names

//				targetPower = new SuperPower(rs.getInt(1), rs.getString(2));
				targetPower = new SuperPower(rs.getInt("spower_id"), rs.getString("spower_name"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return targetPower;
	}


	@Override
	public SuperPower selectPowerByName(String powerName) {
		// target power
		SuperPower targetPower = null;

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {
			String sql = "SELECT * FROM superpowers WHERE spower_name=?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, powerName);

			ResultSet rs = ps.executeQuery(); // <---query not update. this line is what sends the information to the DB

			if (rs.next()) {

				// the following two adds do the same thing, you can extract information using
				// the column orders
				// OR using the column names

//				targetPower = new SuperPower(rs.getInt(1), rs.getString(2));
				targetPower = new SuperPower(rs.getInt("spower_id"), rs.getString("spower_name"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return targetPower;
	}

	
	@Override
	public List<SuperPower> selectAllPowersFromSpecificSuperHuman(String sh_name) {

		// my arraylist
		List<SuperPower> powers = new ArrayList<>();

		try (Connection conn = OurCustomConnectionFactory.getConnection()) {
			String sql = " SELECT *"
					+ " FROM superhumans A"
					+ " INNER JOIN shuman_spower_junction B"
					+ " ON A.shuman_id = B.my_shuman_id"
					+ " INNER JOIN superpowers C"
					+ " ON C.spower_id = B.my_spower_id"
					+ " WHERE A.shuman_name =?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, sh_name);

			ResultSet rs = ps.executeQuery(); // <---query not update. this line is what sends the information to the DB

			while (rs.next()) {

				// the following two adds do the same thing, you can extract information using
				// the column orders
				// OR using the column names

//				powers.add(new SuperPower(rs.getInt(1), rs.getString(2)));
				powers.add(new SuperPower(rs.getInt("spower_id"), rs.getString("spower_name")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return powers;
	}

}
