package demo.menu;

import java.util.Scanner;

import demo.MainDriver;
import demo.model.Account;

public class MenuManager {

	public static void printMainMenu() {
		System.out.println("\n\n\n>>>>>>>>>>MENU OPTIONS<<<<<<<<<<");
		System.out.println(">>>>>You have the following commands you can type in<<<<<");
		System.out.println("1. register");
		System.out.println("2. login");
		System.out.println("0. exit");
		System.out.println();
	}
	
	public static void printLoggedInMenu() {
		System.out.println("\n\n\n>>>>>>>>>>MENU OPTIONS<<<<<<<<<<");
		System.out.println(">>>>>You have the following commands you can type in<<<<<");
		System.out.println("1. select all powers");
		System.out.println("2. select all humans");
		System.out.println( "7. delete human");
		System.out.println("9. space");
		System.out.println("0. logout");
		System.out.println();
	}
	
	public static void loginPrompt(Scanner myScanner) {
		System.out.println("What is your username?");
		String username = myScanner.nextLine();
		
		System.out.println("What is your password?");
		String password = myScanner.nextLine();
		
		if(username.equals("peanutbutter") && password.equals("jelly")) {
			//do NOT hardcode the username and password...go to the database and retrieve the username
			//		and password to check....this is just an example
			//YOu'd have a service method called something like "getUserObjectByUsernameAndPassword"
			// if this method returned and object from your DB query then the username and password
			//	were CLEARLY correct. So they got it right. If your method returns null then they didn't
			//	enter correct and existing credentials.
			
			MainDriver.currentUser = new Account(username, password, "employee");
			
		}else {
			System.out.println("In correct credentials. Try again.");
		}
	}
	
	public static Integer separateMethodForScannerIntInput(Scanner myScan , String prompt) {
		System.out.println(prompt);
				
		try {
			String numberInput = myScan.nextLine();
			int numberConverted = Integer.parseInt(numberInput);
			return numberConverted;
			
		}catch(NumberFormatException e) {
//			e.printStackTrace();
			System.out.println("Sorry, you entered text that is not a number; taking you back to the main menu.");
		}
		
		return null;
	}
}
