package demo.service;

import java.util.List;

import demo.dao.SuperPowerDao;
import demo.dao.SuperPowerDaoImpl;
import demo.model.SuperPower;

public class SuperPowerServiceImpl implements SuperPowerService {

	

	//let's make a Dao object
	SuperPowerDao mySPowerDao = new SuperPowerDaoImpl();
	
	
	
	
	

	@Override
	public boolean addPower(String powerName) {
		//if we DID have business logic it would go here	
		return mySPowerDao.insertPower(powerName);
		//OR it would go here. BY CHANCE, we just don't have any business logic.
	}

	@Override
	public List<SuperPower> findAllPowers() {
		return mySPowerDao.selectAllPowers();
	}

	@Override
	public SuperPower findPowerById(int powerId) {
		return mySPowerDao.selectPowerById(powerId);
	}

	@Override
	public SuperPower findPowerByName(String powerName) {
		return mySPowerDao.selectPowerByName(powerName);
	}

	@Override
	public List<SuperPower> findAllPowersFromSpecificSuperHuman(String superHumanName) {
		return mySPowerDao.selectAllPowersFromSpecificSuperHuman(superHumanName);
	}

}
