package demo.service;

import java.util.List;

import demo.dao.SuperHumanDao;
import demo.dao.SuperHumanDaoImpl;
import demo.model.SuperHuman;

public class SuperHumanServiceImpl implements SuperHumanService {
	
	
	//let's make a Dao object
	SuperHumanDao mySHumanDao = new SuperHumanDaoImpl();
	
	
	

	@Override
	public boolean addHumanWithoutPowers(SuperHuman myHuman) {

		//if we DID have business logic it would go here
//		String lowercaseName = myHuman.getShumanName().toLowerCase();
//		myHuman.setShumanName(lowercaseName);
		
		//HERE you could check to see if the fridge already has 3 items in it
		
		boolean boolResults = mySHumanDao.insertHumanWithoutPowers(myHuman);
		//OR it would go here. BY CHANCE, we just don't have any business logic.
		
		return boolResults;
	}


	@Override
	public boolean addHumanWithPowers(SuperHuman myHuman) {
		return mySHumanDao.insertHumanWithPowers(myHuman);
	}

	@Override
	public List<SuperHuman> findAllHumansMultipleDBQueries() {
		return mySHumanDao.selectAllHumansMultipleDBQueries();
	}

	@Override
	public List<SuperHuman> findAllHumansOneDBQuery() {
		return mySHumanDao.selectAllHumansOneDBQuery();
	}
	
	@Override
	public boolean removeHuman(int targetPrimaryKey) {
	
		if(mySHumanDao.selectHumanById(targetPrimaryKey)==null)
			return false;
		else
			return mySHumanDao.deleteHuman(targetPrimaryKey);
	}

}
