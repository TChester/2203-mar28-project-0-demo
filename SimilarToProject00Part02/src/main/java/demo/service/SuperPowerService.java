package demo.service;

import java.util.List;

import demo.model.SuperPower;

public interface SuperPowerService {

	//inserts
	public boolean addPower(String powerName);
	
	//selects
	public List<SuperPower> findAllPowers();
	public SuperPower findPowerById(int powerId);
	public SuperPower findPowerByName(String powerName);
	public List<SuperPower> findAllPowersFromSpecificSuperHuman(String superHumanName);
}
