package demo.service;

import java.util.List;

import demo.model.SuperHuman;

public interface SuperHumanService {
	//inserts
	public boolean addHumanWithoutPowers(SuperHuman myHuman);
	public boolean addHumanWithPowers(SuperHuman myHuman);
	
	//selects
	public List<SuperHuman> findAllHumansMultipleDBQueries();
	public List<SuperHuman> findAllHumansOneDBQuery();
	
	//deletes
	public boolean removeHuman(int targetPrimaryKey);
}
